<?php
   require("conDB.php");
  //  $sql = " SELECT id,node_id,create_dete,temperature,pressure,pm1,pm2_5,pm10,data 
  //           FROM `tb_results` 
  //           where DATE_FORMAT(create_dete, '%d/%m/%Y') =  DATE_FORMAT(CURDATE(), '%d/%m/%Y')
  //           ORDER BY `create_dete` DESC";
    $sql = "SELECT id,node_id,create_dete,temperature,pressure,pm1,pm2_5,pm10,data 
            FROM `tb_results` 
            ORDER BY `create_dete` DESC LIMIT 0,1";
   $result = mysqli_query($link,$sql);
   $date = [];
   $temperature = [];
   $pressure = [];
   $pm1 = [];
   $pm2_5 = [];
   $pm10 = [];
   $tmp = [];
   if($result){
       if($row = mysqli_fetch_array($result))
       {
        array_push($date,$row['create_dete']);
        array_push($temperature,((float) $row['temperature']));
        array_push($pressure,((float)$row['pressure']));
        array_push($pm1,((float)$row['pm1']));
        array_push($pm2_5,((float)$row['pm2_5']));
        array_push($pm10,((float) $row['pm10']));
        //echo json_encode($row['pm10']);
  
       }
       array_push($tmp,$date,$temperature,$pressure,$pm1,$pm2_5,$pm10);
       echo json_encode($tmp);
   }
  mysqli_close($link);
?>