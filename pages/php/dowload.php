<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=export.xls");

/*********GetData*********** */
require("conDB.php");
$dateFrom = @$_GET['dateFrom'];
$dateTo = @$_GET['dateTo'];
$location =  @$_GET['location'];

$sql = "SELECT tr.*,tl.location_name
FROM tb_results tr INNER JOIN tb_location tl on tr.node_id = tl.id
WHERE  DATE_FORMAT(tr.create_dete, ('%d-%m-%Y')) BETWEEN '$dateFrom' AND '$dateTo'  AND tr.node_id = $location
ORDER BY `create_dete` ASC";

?>
<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
		<title>มหาวิทยาลัยราชภัฎบุรีรัมย์</title>
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="../../js/bootstrap-4.0.0/css/bootstrap.min.css" />
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<br /><br /><br />
					<h4> รายงานแสดงผลข้อมูลรายสัปดาห์</h4>
					<p><a href="?act=excel" class="btn btn-primary"> Export->Excel </a></p>
					<table  class="table table-bordered">
						<thead>
							<tr class="info">
								<th>สถานที</th>
								<th>วันที่สร้าง</th>
								<th>temperature</th>
                                <th>pressure</th>
								<th>pm1</th>
                                <th>pm2.5</th>
                                <th>pm10</th>
							</tr>
						</thead>
                        <tbody>
                        <?php  
                            $result = mysqli_query($link,$sql);
                            if($result){
                                while ($row = mysqli_fetch_array($result)) {
                        ?>
                                <tr>
                                    <td><?=$row['location_name'].' ('.$row['node_id'].')'  ?></td>
                                    <td><?=$row['create_dete']?></td>
                                    <td><?=$row['temperature']?></td>
                                    <td><?=$row['pressure']?></td>
                                    <td><?=$row['pm1']?></td>
                                    <td><?=$row['pm2_5']?></td>
                                    <td><?=$row['pm10']?></td> 
                                </tr>
                                <?php
                                }
                            }
                        ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>