-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2021 at 03:55 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `results_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_results`
--

CREATE TABLE `tb_results` (
  `id` int(11) NOT NULL,
  `node_id` varchar(25) NOT NULL,
  `create_dete` datetime NOT NULL,
  `temperature` decimal(10,2) NOT NULL,
  `pressure` decimal(10,2) NOT NULL,
  `pm1` decimal(10,2) NOT NULL,
  `pm2_5` decimal(10,2) NOT NULL,
  `pm10` decimal(10,2) NOT NULL,
  `data` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_results`
--

INSERT INTO `tb_results` (`id`, `node_id`, `create_dete`, `temperature`, `pressure`, `pm1`, `pm2_5`, `pm10`, `data`) VALUES
(1, '1', '2021-12-01 18:26:00', '10.30', '60.00', '12.00', '15.00', '20.00', '[10.30,60.00,12.00,15.00,20.00]'),
(2, '1', '2021-12-02 18:27:00', '10.02', '10.00', '12.00', '62.00', '15.00', '[10.02,10.00,12.00,62.00,15.00]'),
(3, '1', '2021-12-03 18:27:00', '10.00', '9.00', '11.00', '50.00', '14.00', '[10.00,9.00,11.00,50.00,14.00]'),
(4, '1', '2021-12-09 12:00:00', '10.03', '60.00', '12.00', '15.00', '20.00', '[10.03,60.00,12.00,15.00,20.00]'),
(5, '1', '2021-12-10 12:00:00', '12.00', '25.00', '36.00', '25.00', '36.00', '[12.00,25.00,36.00,25.00,36.00]'),
(6, '1', '2021-12-11 12:00:00', '9.30', '10.22', '12.50', '12.22', '2.00', '[9.30,10.22,12.50,12.22,2.00]'),
(7, '1', '2021-12-05 12:00:00', '12.00', '25.00', '36.00', '25.00', '20.00', '[12.00,25.00,36.00,25.00,20.00]'),
(8, '1', '2021-12-06 10:43:12', '6.00', '12.30', '12.50', '15.00', '20.00', '[5.00,12.30,12.30,15.00,20.00]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_results`
--
ALTER TABLE `tb_results`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_results`
--
ALTER TABLE `tb_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
